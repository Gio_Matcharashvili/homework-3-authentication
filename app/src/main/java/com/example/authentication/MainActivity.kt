package com.example.authentication
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_authentication.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        init()
    }
    private fun init(){
        signInButton.setOnClickListener{
            val InIntent = Intent(this, SignInActivity::class.java)
            startActivity(InIntent)
        }
        signUpButton.setOnClickListener{
            val UpIntent = Intent(this, SignUpActivity::class.java)
            startActivity(UpIntent)
        }
    }

}